# quinielas-core

An automated solution to result tables in lottery agencies' posters.

This project have the core requirements and API.
The official frontend is at <https://gitlab.com/chexmo/quinielas-front>

## Usage

Remind to run the following command in the root folder before running.

```shell
npm install 
```

To deploy use

```npm
npm run start
```

To start while development, with auto-compile and logging

```npm
npm run dev
```

## Dependencies

Project developed using

* Node 6.x and npm 14.17.0
* MongoDB 4.4.6

There are more dependencies that will automatically install running a `npm install` as said above.
