"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const juego_routes_1 = __importDefault(require("./routes/juego.routes"));
const coleccion_routes_1 = __importDefault(require("./routes/coleccion.routes"));
const personalizados_routes_1 = __importDefault(require("./routes/personalizados.routes"));
//inicializacion
const app = express_1.default();
//setting
app.set("port", process.env.PORT || 3000);
//middlewares
app.use(morgan_1.default("dev"));
app.use(cors_1.default());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(express_1.default.json());
app.get("/", (req, res) => {
    res.send(`La API se inicio en http://localhost:${app.get("port")}`);
});
app.use(juego_routes_1.default);
app.use(coleccion_routes_1.default);
app.use(personalizados_routes_1.default);
// const endpoints: any = app._router.stack
//   .filter(
//     (x: any) => x.route && x.route.path && Object.keys(x.route.methods) != 0
//   )
//   .map((layer: any) => ({
//     method: layer.route.stack[0].method.toUpperCase(),
//     path: layer.route.path,
//   }));
// console.log(endpoints);
exports.default = app;
