"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDateTitle = exports.addDateTitle = void 0;
const dateTitle_1 = __importDefault(require("../models/dateTitle"));
const addDateTitle = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.title || !req.body.date) {
        return res.status(400).json({
            msg: "Por favor. Escriba los datos correctamente.",
        });
    }
    const dateTitle = yield dateTitle_1.default.find({ title: req.body.title });
    console.log(dateTitle);
    // if (dateTitle) {
    //   return res.status(400).json({ msg: "El titulo ya existe" });
    // }
    const newDateTitle = new dateTitle_1.default(req.body);
    yield newDateTitle.save();
    return res.status(201).json(newDateTitle);
});
exports.addDateTitle = addDateTitle;
const getDateTitle = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const dateTitle = yield dateTitle_1.default.find();
    return res.status(201).json(dateTitle);
});
exports.getDateTitle = getDateTitle;
