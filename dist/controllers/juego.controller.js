"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updatePorTurnoJuegoActual = exports.getJuegosLastJugadaPorTurno = exports.getJuegoConJugadasPaginadas = exports.getJugadasPaginadas = exports.getSoloJuego = exports.getSoloJuegos = exports.deleteJugada = exports.deleteJuego = exports.getLastJuego = exports.getJuego = exports.updateCreateLastGame = exports.updateJuego = exports.getJuegos = exports.addJuego = void 0;
const juego_model_1 = __importDefault(require("../models/juego.model"));
const mongoose = require("mongoose");
/** Añadir un Juego */
const addJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juego = yield juego_model_1.default.findOne({
        nombre: req.body.nombre,
        turno: req.body.turno,
    });
    if (juego) {
        return res.status(400).json({ mgs: "El juego ya existe" });
    }
    const newJuego = new juego_model_1.default(req.body);
    yield newJuego.save();
    return res.status(201).json(newJuego);
});
exports.addJuego = addJuego;
/** Obtener los Juegos */
const getJuegos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juegos = yield juego_model_1.default.find();
    return res.status(201).json(juegos);
});
exports.getJuegos = getJuegos;
/** Actualizar los Juegos */
const updateJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juego = yield juego_model_1.default.findByIdAndUpdate(req.params.id, req.body, {
        omitUndefined: true,
    });
    return res.status(201).json(juego);
});
exports.updateJuego = updateJuego;
/** Crear o Actualizar Ultima Jugada de un Juego */
const updateCreateLastGame = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const searchJuego = yield juego_model_1.default.findOne({
        "jugadas.fecha": req.body.fecha,
        _id: req.params.id,
    });
    if (searchJuego) {
        const juego = yield juego_model_1.default.updateOne({ "jugadas.fecha": req.body.fecha, _id: req.params.id }, {
            $set: { "jugadas.$": req.body },
        });
        return res.status(201).json(juego);
    }
    const juego = yield juego_model_1.default.findByIdAndUpdate(req.params.id, {
        $push: { jugadas: { $each: [req.body], $position: 0 } },
    });
    return res.status(201).json(juego);
});
exports.updateCreateLastGame = updateCreateLastGame;
/** Obtener un solo Juego */
const getJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juego = yield juego_model_1.default.findById(req.params.id);
    return res.status(201).json(juego);
});
exports.getJuego = getJuego;
/** Obtener la ultima Jugada de un Juego */
const getLastJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.params.id.length != 24)
        return res.status(400).json({ message: "Invalid id" });
    var juego = yield juego_model_1.default.aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(req.params.id),
            },
        },
        { $unwind: "$jugadas" },
        { $sort: { "jugadas.fecha": -1 } },
        {
            $group: {
                _id: "$_id",
                nombre: { $first: "$nombre" },
                jugadas: { $first: "$jugadas" },
            },
        },
    ]);
    if (juego[0] != null && juego[0] != undefined) {
        if ("jugadas" in juego[0] && juego[0].jugadas != null) {
            return res.status(201).json(juego[0].jugadas);
        }
    }
    return res.status(404).json({ message: "Jugada no encontrada" });
});
exports.getLastJuego = getLastJuego;
/** Eliminar un Juego */
const deleteJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const deleteJuego = yield juego_model_1.default.findByIdAndDelete(req.params.id);
    return res.status(201).json(deleteJuego);
});
exports.deleteJuego = deleteJuego;
/** Eliminar una Jugada */
const deleteJugada = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const searchJuego = yield juego_model_1.default.findById(req.params.id);
    if (searchJuego) {
        const deleteJugada = yield juego_model_1.default.updateOne({ _id: req.params.id }, { $pull: { jugadas: { fecha: req.body.fecha } } });
        return res.status(201).json(deleteJugada);
    }
    else {
        return res.status(201).json("No existe esa jugada");
    }
});
exports.deleteJugada = deleteJugada;
/** Obtener todos los juegos pero sin las jugadas */
const getSoloJuegos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juegos = yield juego_model_1.default.find().select(["-jugadas"]);
    return res.status(201).json(juegos);
});
exports.getSoloJuegos = getSoloJuegos;
/** Obtener un juego por el id pero sin las jugadas */
const getSoloJuego = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juego = yield juego_model_1.default.findById(req.params.id).select(["-jugadas"]);
    return res.status(201).json(juego);
});
exports.getSoloJuego = getSoloJuego;
const getJugadasPaginadas = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.params.id.length != 24)
        return res.status(400).json({ message: "Invalid id" });
    var desde = req.query["desde"];
    var cant = req.query["cant"];
    if (typeof desde !== "string" || typeof cant !== "string") {
        return res.status(400).json({ message: "Invalid variables" });
    }
    var juego = yield juego_model_1.default.aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(req.params.id),
            },
        },
        { $unwind: "$jugadas" },
        { $sort: { "jugadas.fecha": -1 } },
        { $skip: parseInt(desde) },
        { $limit: parseInt(cant) },
    ]);
    if (juego == null || juego == undefined || juego.length == 0) {
        return res.status(404).json({ message: "Nothing found in database" });
    }
    var jugEncontradas = [];
    juego.forEach((j) => {
        if ("jugadas" in j) {
            jugEncontradas.push(j.jugadas);
        }
    });
    return res.status(200).json(jugEncontradas);
});
exports.getJugadasPaginadas = getJugadasPaginadas;
const getJuegoConJugadasPaginadas = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.params.id.length != 24)
        return res.status(400).json({ message: "Invalid id" });
    var desde = req.query["desde"];
    var cant = req.query["cant"];
    if (typeof desde !== "string" || typeof cant !== "string") {
        return res.status(400).json({ message: "Invalid variables" });
    }
    var juego = yield juego_model_1.default.aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(req.params.id),
            },
        },
        { $unwind: "$jugadas" },
        { $sort: { "jugadas.fecha": -1 } },
        { $skip: parseInt(desde) },
        { $limit: parseInt(cant) },
    ]);
    if (juego == null || juego == undefined || juego.length == 0) {
        return res.status(404).json({ message: "Nothing found in database" });
    }
    var jugEncontradas = [];
    juego.forEach((j) => {
        if ("jugadas" in j) {
            jugEncontradas.push(j.jugadas);
        }
    });
    var juegoResp = juego[0];
    juegoResp.jugadas = jugEncontradas;
    return res.status(200).json(juegoResp);
});
exports.getJuegoConJugadasPaginadas = getJuegoConJugadasPaginadas;
/** Obtener todos los juegos con la ultima jugada por el turno */
const getJuegosLastJugadaPorTurno = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var juego = yield juego_model_1.default.aggregate([
        {
            $match: {
                turno: req.params.turno,
            },
        },
        { $unwind: "$jugadas" },
        { $sort: { "jugadas.fecha": -1 } },
        {
            $group: {
                _id: "$_id",
                nombre: { $first: "$nombre" },
                turno: { $first: "$turno" },
                horaDelSorteo: { $first: "$horaDelSorteo" },
                jugadas: { $first: "$jugadas" },
            },
        },
    ]);
    if (juego.length > 0) {
        return res.status(200).json(juego);
    }
    return res.status(404).json({ message: `Juegos con el turno ${req.params.turno}, no encontrados` });
});
exports.getJuegosLastJugadaPorTurno = getJuegosLastJugadaPorTurno;
/* Actualizar todos los juegos dato un turno */
const updatePorTurnoJuegoActual = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const juego = yield juego_model_1.default.updateMany({ "turno": req.params.turno }, {
        $set: { "juegoActual": true },
    });
    yield juego_model_1.default.updateMany({ "turno": { $ne: req.params.turno } }, {
        $set: { "juegoActual": false },
    });
    return res.status(201).json(juego);
});
exports.updatePorTurnoJuegoActual = updatePorTurnoJuegoActual;
