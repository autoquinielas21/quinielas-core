"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
require("./database");
const GameplayCreator_service_1 = require("./services/GameplayCreator.service");
const Isomnia_service_1 = require("./services/Isomnia.service");
app_1.default.listen(app_1.default.get("port"));
console.log("Server on port", app_1.default.get("port"));
Isomnia_service_1.Imsomnia.keepWorking();
const createGames = new GameplayCreator_service_1.GameplayCreator();
createGames.loop();
