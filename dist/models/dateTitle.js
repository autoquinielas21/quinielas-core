"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const dateTitleSchema = new mongoose_1.Schema({
    title: {
        type: String,
        require: true,
    },
    date: {
        type: Date,
        timestamps: true,
    },
});
exports.default = mongoose_1.model("DateTitle", dateTitleSchema);
