"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const jugadas_model_1 = require("./jugadas.model");
const juegoSchema = new mongoose_1.Schema({
    nombre: {
        type: String,
        require: true,
    },
    turno: {
        type: String,
        require: true,
    },
    logo: {
        type: String,
        require: true,
    },
    horaDelSorteo: {
        type: String,
        require: true,
    },
    vigente: {
        type: Boolean,
        require: true,
    },
    juegoActual: {
        type: Boolean,
        default: false,
        require: false,
    },
    jugadas: [jugadas_model_1.jugadasSchema],
}, {
    timestamps: true,
    versionKey: false,
});
const año = new Date().getFullYear();
exports.default = mongoose_1.model("Juego-" + año.toString(), juegoSchema);
