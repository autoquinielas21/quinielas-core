"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jugadasSchema = exports.JugadaImpl = void 0;
const mongoose_1 = require("mongoose");
class JugadaImpl {
    constructor(fecha) {
        this.fecha = fecha;
        this.puesto1 = null;
        this.puesto2 = null;
        this.puesto3 = null;
        this.puesto4 = null;
        this.puesto5 = null;
        this.puesto6 = null;
        this.puesto7 = null;
        this.puesto8 = null;
        this.puesto9 = null;
        this.puesto10 = null;
        this.puesto11 = null;
        this.puesto12 = null;
        this.puesto13 = null;
        this.puesto14 = null;
        this.puesto15 = null;
        this.puesto16 = null;
        this.puesto17 = null;
        this.puesto18 = null;
        this.puesto19 = null;
        this.puesto20 = null;
    }
}
exports.JugadaImpl = JugadaImpl;
exports.jugadasSchema = new mongoose_1.Schema({
    fecha: {
        type: Date,
        require: true,
    },
    puesto1: {
        type: Number,
        require: false,
    },
    puesto2: {
        type: Number,
        require: false,
    },
    puesto3: {
        type: Number,
        require: false,
    },
    puesto4: {
        type: Number,
        require: false,
    },
    puesto5: {
        type: Number,
        require: false,
    },
    puesto6: {
        type: Number,
        require: false,
    },
    puesto7: {
        type: Number,
        require: false,
    },
    puesto8: {
        type: Number,
        require: false,
    },
    puesto9: {
        type: Number,
        require: false,
    },
    puesto10: {
        type: Number,
        require: false,
    },
    puesto11: {
        type: Number,
        require: false,
    },
    puesto12: {
        type: Number,
        require: false,
    },
    puesto13: {
        type: Number,
        require: false,
    },
    puesto14: {
        type: Number,
        require: false,
    },
    puesto15: {
        type: Number,
        require: false,
    },
    puesto16: {
        type: Number,
        require: false,
    },
    puesto17: {
        type: Number,
        require: false,
    },
    puesto18: {
        type: Number,
        require: false,
    },
    puesto19: {
        type: Number,
        require: false,
    },
    puesto20: {
        type: Number,
        require: false,
    },
}, {
    _id: false,
});
