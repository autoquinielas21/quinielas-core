"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const dateTitle_controller_1 = require("../controllers/dateTitle.controller");
router.post("/horatitulo", dateTitle_controller_1.addDateTitle);
router.get("/horatitulo", dateTitle_controller_1.getDateTitle);
exports.default = router;
