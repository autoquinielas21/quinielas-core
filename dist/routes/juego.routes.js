"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const juego_controller_1 = require("../controllers/juego.controller");
const express_1 = require("express");
const router = express_1.Router();
// Get Juegos
router.get("/juego", juego_controller_1.getJuegos);
router.get("/juego/:id", juego_controller_1.getJuego);
router.get("/soloJuegos", juego_controller_1.getSoloJuegos);
router.get("/soloJuego/:id", juego_controller_1.getSoloJuego);
// Get Jugadas
router.get("/juego/getLastJuego/:id", juego_controller_1.getLastJuego);
// Post Juegos
router.post("/juego", juego_controller_1.addJuego);
// Put Juegos
router.put("/juego/:id", juego_controller_1.updateJuego);
// Put Jugadas
router.put("/addUltimoJuego/:id", juego_controller_1.updateCreateLastGame);
// Delete Juegos
router.delete("/juego/:id", juego_controller_1.deleteJuego);
// Delete Jugadas
router.delete("/juego/:id/jugada", juego_controller_1.deleteJugada);
router.get("/jugadas/paginadas/:id?", juego_controller_1.getJugadasPaginadas);
router.get("/juego/jugadasPaginadas/:id?", juego_controller_1.getJuegoConJugadasPaginadas);
router.get("/juego/getAllJuego/:turno", juego_controller_1.getJuegosLastJugadaPorTurno);
router.put("/juego/updateActualJuego/:turno", juego_controller_1.updatePorTurnoJuegoActual);
exports.default = router;
