import express from "express";
import morgan from "morgan";
import cors from "cors";

import juegoRouter from "./routes/juego.routes";
import coleccionRouter from "./routes/coleccion.routes";
import personalizadosRouter from "./routes/personalizados.routes";

//inicializacion
const app = express();

//setting
app.set("port", process.env.PORT || 3000);

//middlewares
app.use(morgan("dev"));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get("/", (req, res) => {
  res.send(`La API se inicio en http://localhost:${app.get("port")}`);
});

app.use(juegoRouter);
app.use(coleccionRouter);
app.use(personalizadosRouter);
// const endpoints: any = app._router.stack
//   .filter(
//     (x: any) => x.route && x.route.path && Object.keys(x.route.methods) != 0
//   )
//   .map((layer: any) => ({
//     method: layer.route.stack[0].method.toUpperCase(),
//     path: layer.route.path,
//   }));

// console.log(endpoints);

export default app;
