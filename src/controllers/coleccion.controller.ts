import { Request, Response } from "express";
import mongoose from "mongoose";

export const juegoColeccion = async (req: Request, res: Response) => {
  let colecionJuego: any[] = [];

  mongoose.connection.db.collection(
    "juego-" + req.query.año,
    (err, collection) => {
      collection
        .find()
        .toArray()
        .then((x) => {
          colecionJuego = x;
          if (colecionJuego.length <= 0) {
            return res.status(201).json("no existe esa coleccion");
          }
          return res.status(201).json(colecionJuego);
        });
    }
  );
};
