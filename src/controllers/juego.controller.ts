import { Request, Response } from "express";
import Juego from "../models/juego.model";
import mongoose = require("mongoose");

/** Añadir un Juego */
export const addJuego = async (req: Request, res: Response) => {
  const juego = await Juego.findOne({
    nombre: req.body.nombre,
    turno: req.body.turno,
  });
  if (juego) {
    return res.status(400).json({ mgs: "El juego ya existe" });
  }
  const newJuego = new Juego(req.body);
  await newJuego.save();
  return res.status(201).json(newJuego);
};

/** Obtener los Juegos */
export const getJuegos = async (req: Request, res: Response) => {
  const juegos = await Juego.find();
  return res.status(201).json(juegos);
};

/** Actualizar los Juegos */
export const updateJuego = async (req: Request, res: Response) => {
  const juego = await Juego.findByIdAndUpdate(req.params.id, req.body, {
    omitUndefined: true,
  });
  return res.status(201).json(juego);
};

/** Crear o Actualizar Ultima Jugada de un Juego */
export const updateCreateLastGame = async (req: Request, res: Response) => {
  const searchJuego = await Juego.findOne({
    "jugadas.fecha": req.body.fecha,
    _id: req.params.id,
  });
  if (searchJuego) {
    const juego = await Juego.updateOne(
      { "jugadas.fecha": req.body.fecha, _id: req.params.id },
      {
        $set: { "jugadas.$": req.body },
      }
    );
    return res.status(201).json(juego);
  }
  const juego = await Juego.findByIdAndUpdate(req.params.id, {
    $push: { jugadas: { $each: [req.body], $position: 0 } },
  });
  return res.status(201).json(juego);
};

/** Obtener un solo Juego */
export const getJuego = async (req: Request, res: Response) => {
  const juego = await Juego.findById(req.params.id);
  return res.status(201).json(juego);
};

/** Obtener la ultima Jugada de un Juego */
export const getLastJuego = async (req: Request, res: Response) => {
  if (req.params.id.length != 24)
    return res.status(400).json({ message: "Invalid id" });

  var juego = await Juego.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    { $unwind: "$jugadas" },
    { $sort: { "jugadas.fecha": -1 } },
    {
      $group: {
        _id: "$_id",
        nombre: { $first: "$nombre" },
        jugadas: { $first: "$jugadas" },
      },
    },
  ]);

  if (juego[0] != null && juego[0] != undefined) {
    if ("jugadas" in juego[0] && juego[0].jugadas != null) {
      return res.status(201).json(juego[0].jugadas);
    }
  }

  return res.status(404).json({ message: "Jugada no encontrada" });
};

/** Eliminar un Juego */
export const deleteJuego = async (req: Request, res: Response) => {
  const deleteJuego = await Juego.findByIdAndDelete(req.params.id);
  return res.status(201).json(deleteJuego);
};

/** Eliminar una Jugada */
export const deleteJugada = async (req: Request, res: Response) => {
  const searchJuego = await Juego.findById(req.params.id);
  if (searchJuego) {
    const deleteJugada = await Juego.updateOne(
      { _id: req.params.id },
      { $pull: { jugadas: { fecha: req.body.fecha } } }
    );
    return res.status(201).json(deleteJugada);
  } else {
    return res.status(201).json("No existe esa jugada");
  }
};

/** Obtener todos los juegos pero sin las jugadas */
export const getSoloJuegos = async (req: Request, res: Response) => {
  const juegos = await Juego.find().select(["-jugadas"]);
  return res.status(201).json(juegos);
};

/** Obtener un juego por el id pero sin las jugadas */
export const getSoloJuego = async (req: Request, res: Response) => {
  const juego = await Juego.findById(req.params.id).select(["-jugadas"]);
  return res.status(201).json(juego);
};

export const getJugadasPaginadas = async (req: Request, res: Response) => {
  if (req.params.id.length != 24)
    return res.status(400).json({ message: "Invalid id" });

  var desde = req.query["desde"];
  var cant = req.query["cant"];

  if (typeof desde !== "string" || typeof cant !== "string") {
    return res.status(400).json({ message: "Invalid variables" });
  }

  var juego = await Juego.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    { $unwind: "$jugadas" },
    { $sort: { "jugadas.fecha": -1 } },
    { $skip: parseInt(desde) },
    { $limit: parseInt(cant) },
  ]);

  if (juego == null || juego == undefined || juego.length == 0) {
    return res.status(404).json({ message: "Nothing found in database" });
  }

  var jugEncontradas: any[] = [];
  juego.forEach((j) => {
    if ("jugadas" in j) {
      jugEncontradas.push(j.jugadas);
    }
  });

  return res.status(200).json(jugEncontradas);
};

export const getJuegoConJugadasPaginadas = async (
  req: Request,
  res: Response
) => {
  if (req.params.id.length != 24)
    return res.status(400).json({ message: "Invalid id" });

  var desde = req.query["desde"];
  var cant = req.query["cant"];

  if (typeof desde !== "string" || typeof cant !== "string") {
    return res.status(400).json({ message: "Invalid variables" });
  }

  var juego = await Juego.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    { $unwind: "$jugadas" },
    { $sort: { "jugadas.fecha": -1 } },
    { $skip: parseInt(desde) },
    { $limit: parseInt(cant) },
  ]);

  if (juego == null || juego == undefined || juego.length == 0) {
    return res.status(404).json({ message: "Nothing found in database" });
  }

  var jugEncontradas: any[] = [];
  juego.forEach((j) => {
    if ("jugadas" in j) {
      jugEncontradas.push(j.jugadas);
    }
  });

  var juegoResp = juego[0];
  juegoResp.jugadas = jugEncontradas;

  return res.status(200).json(juegoResp);
};

/** Obtener todos los juegos con la ultima jugada por el turno */
export const getJuegosLastJugadaPorTurno = async (req: Request, res: Response) => {
  var juego = await Juego.aggregate([
    {
      $match: {
        turno: req.params.turno,
      },
    },
    { $unwind: "$jugadas" },
    { $sort: { "jugadas.fecha": -1 } },
    {
      $group: {
        _id: "$_id",
        nombre: { $first: "$nombre" },
        turno: { $first: "$turno" },
        horaDelSorteo: { $first: "$horaDelSorteo" },
        jugadas: { $first: "$jugadas" },
      },
    },
  ]);

  if (juego.length > 0) {
    return res.status(200).json(juego);
  }
  
  return res.status(404).json({ message: `Juegos con el turno ${req.params.turno}, no encontrados` });
}

/* Actualizar todos los juegos dato un turno */ 
export const updatePorTurnoJuegoActual = async (req: Request, res: Response) => {
  
  const juego = await Juego.updateMany(
    { "turno": req.params.turno },
    {
      $set: { "juegoActual": true },
    }
  );
  await Juego.updateMany(
    { "turno": { $ne: req.params.turno } },
    {
      $set: { "juegoActual": false },
    }
  );

  return res.status(201).json(juego);
  
}