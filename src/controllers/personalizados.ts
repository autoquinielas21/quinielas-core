import { Request, Response } from "express";
import Personalizados from "../models/personalizados.model";

/** Crear o Actualizar un Personalizado */
export const updateCreatePersonalizado = async (req: Request, res: Response) => {
  const searchPersonalizados = await Personalizados.findOne({
    parametroId: req.body.parametroId,
  });
  if (searchPersonalizados) {
    const personalizada = await Personalizados.updateOne(
      { parametroId: req.body.parametroId },
      {
        $set: req.body,
      }
    );
    return res.status(201).json(personalizada);
  }
  const newPersonalizado = new Personalizados(req.body);
  newPersonalizado.save();
  return res.status(201).json(newPersonalizado);
};

/** Obtener los Personalizados */
export const getPersonalizados = async (req: Request, res: Response) => {
  const personalizados = await Personalizados.find();
  return res.status(201).json(personalizados);
};

/* obtener Personalizados por el parameterId */
export const getPersonalizadoByParameterId = async (req: Request, res: Response) => {
  var personalizado = await Personalizados.aggregate([
    {
      $match: {
        parametroId: req.params.parametroId,
      },
    },
    {
      $group: {
        _id: "$_id",
        parametroId: { $first: "$parametroId" },
        tituloUnoCarteleraUno: { $first: "$tituloUnoCarteleraUno" },
        tituloDosCarteleraUno: { $first: "$tituloDosCarteleraUno" },
        imagenCarteleraUno: { $first: "$imagenCarteleraUno" },
        tituloUnoCarteleraDos: { $first: "$tituloUnoCarteleraDos" },
        tituloDosCarteleraDos: { $first: "$tituloDosCarteleraDos" },
      },
    },
  ]);

  if (personalizado.length > 0) {
    return res.status(200).json(personalizado);
  }

  return res.status(404).json({ message: `El Personalizado con parameterId: ${req.params.parametroId}, no existe!` });
};

/* Elimina un personalizado por el id */
export const deletePersonalizado = async (req: Request, res: Response) => {
  const deletePersonalizado = await Personalizados.findByIdAndDelete(req.params.id);
  return res.status(201).json(deletePersonalizado);
}