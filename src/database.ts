import mongoose, { ConnectOptions } from "mongoose";
require("dotenv").config({ path: "variables.env" });

const dbOptions: ConnectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

mongoose
  .connect(process.env.DB_URL!, dbOptions)
  .then(() => console.log("Database Connected"))
  .catch((err) => console.log(err));
