import app from "./app";
import "./database";
import { GameplayCreator } from "./services/GameplayCreator.service";
import { Imsomnia } from "./services/Isomnia.service";

app.listen(app.get("port"));
console.log("Server on port", app.get("port"));

Imsomnia.keepWorking();
const createGames = new GameplayCreator();
createGames.loop();
