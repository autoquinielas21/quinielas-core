import { model, Schema, Document } from "mongoose";
import { IJugadas, jugadasSchema } from "./jugadas.model";

export interface IJuego extends Document {
  nombre: String; // Quiniela Nacional
  turno: String; // Vespertina, Matutina, Primera, etc
  logo: String;
  horaDelSorteo: String; // 12:00hs
  vigente: Boolean; // si sortea es true
  juegoActual: Boolean; // si esta en false no se muestra en cartelera actual, sino si.
  jugadas?: IJugadas[]; //...
}

const juegoSchema = new Schema<IJuego>(
  {
    nombre: {
      type: String,
      require: true,
    },
    turno: {
      type: String,
      require: true,
    },
    logo: {
      type: String,
      require: true,
    },
    horaDelSorteo: {
      type: String,
      require: true,
    },
    vigente: {
      type: Boolean,
      require: true,
    },
    juegoActual: {
      type: Boolean,
      default: false,
      require: false,
    },
    jugadas: [jugadasSchema],
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

const año = new Date().getFullYear();

export default model<IJuego>("Juego-" + año.toString(), juegoSchema);
