import { Schema } from "mongoose";

export interface IJugadas {
  fecha: Date;
  puesto1?: number;
  puesto2?: number;
  puesto3?: number;
  puesto4?: number;
  puesto5?: number;
  puesto6?: number;
  puesto7?: number;
  puesto8?: number;
  puesto9?: number;
  puesto10?: number;
  puesto11?: number;
  puesto12?: number;
  puesto13?: number;
  puesto14?: number;
  puesto15?: number;
  puesto16?: number;
  puesto17?: number;
  puesto18?: number;
  puesto19?: number;
  puesto20?: number;
}

export class JugadaImpl implements IJugadas {
  fecha!: Date;
  puesto1?: any;
  puesto2?: any;
  puesto3?: any;
  puesto4?: any;
  puesto5?: any;
  puesto6?: any;
  puesto7?: any;
  puesto8?: any;
  puesto9?: any;
  puesto10?: any;
  puesto11?: any;
  puesto12?: any;
  puesto13?: any;
  puesto14?: any;
  puesto15?: any;
  puesto16?: any;
  puesto17?: any;
  puesto18?: any;
  puesto19?: any;
  puesto20?: any;

  constructor(fecha: Date) {
    this.fecha = fecha;
    this.puesto1 = null;
    this.puesto2 = null;
    this.puesto3 = null;
    this.puesto4 = null;
    this.puesto5 = null;
    this.puesto6 = null;
    this.puesto7 = null;
    this.puesto8 = null;
    this.puesto9 = null;
    this.puesto10 = null;
    this.puesto11 = null;
    this.puesto12 = null;
    this.puesto13 = null;
    this.puesto14 = null;
    this.puesto15 = null;
    this.puesto16 = null;
    this.puesto17 = null;
    this.puesto18 = null;
    this.puesto19 = null;
    this.puesto20 = null;
  }
}

export const jugadasSchema = new Schema<IJugadas>(
  {
    fecha: {
      type: Date,
      require: true,
    },
    puesto1: {
      type: Number,
      require: false,
    },
    puesto2: {
      type: Number,
      require: false,
    },
    puesto3: {
      type: Number,
      require: false,
    },
    puesto4: {
      type: Number,
      require: false,
    },
    puesto5: {
      type: Number,
      require: false,
    },
    puesto6: {
      type: Number,
      require: false,
    },
    puesto7: {
      type: Number,
      require: false,
    },
    puesto8: {
      type: Number,
      require: false,
    },
    puesto9: {
      type: Number,
      require: false,
    },
    puesto10: {
      type: Number,
      require: false,
    },
    puesto11: {
      type: Number,
      require: false,
    },
    puesto12: {
      type: Number,
      require: false,
    },
    puesto13: {
      type: Number,
      require: false,
    },
    puesto14: {
      type: Number,
      require: false,
    },
    puesto15: {
      type: Number,
      require: false,
    },
    puesto16: {
      type: Number,
      require: false,
    },
    puesto17: {
      type: Number,
      require: false,
    },
    puesto18: {
      type: Number,
      require: false,
    },
    puesto19: {
      type: Number,
      require: false,
    },
    puesto20: {
      type: Number,
      require: false,
    },
  },
  {
    _id: false,
  }
);

export default IJugadas;
