import { Schema, Document, model } from "mongoose";

export interface IPersonzalizados extends Document {
    parametroId: String;
    tituloUnoCarteleraUno: String;
    tituloDosCarteleraUno: String; 
    imagenCarteleraUno: String;
    tituloUnoCarteleraDos: String; 
    tituloDosCarteleraDos: String; 
}

const PersonalizadosSchema = new Schema<IPersonzalizados>(
    {
        parametroId: {
            type: String,
            require: true,
        },
        tituloUnoCarteleraUno: {
            type: String,
            require: false,
        },
        tituloDosCarteleraUno: {
            type: String,
            require: false,
        },
        imagenCarteleraUno: {
            type: String,
            require: false,
        },
        tituloUnoCarteleraDos: {
            type: String,
            require: false,
        },
        tituloDosCarteleraDos: {
            type: String,
            require: false,
        },
    },
)

export default model<IPersonzalizados>("Personzaliados", PersonalizadosSchema);