import { juegoColeccion } from "../controllers/coleccion.controller";
import { Router } from "express";
const router = Router();

// Get Colecciones
router.get("/coleccion/", juegoColeccion);

export default router;
