import {
  addJuego,
  getJuegos,
  updateJuego,
  getJuego,
  updateCreateLastGame,
  getLastJuego,
  deleteJuego,
  deleteJugada,
  getSoloJuegos,
  getSoloJuego,
  getJuegoConJugadasPaginadas,
  getJugadasPaginadas,
  getJuegosLastJugadaPorTurno,
  updatePorTurnoJuegoActual
} from "../controllers/juego.controller";
import { Router } from "express";
const router = Router();

// Get Juegos
router.get("/juego", getJuegos);
router.get("/juego/:id", getJuego);
router.get("/soloJuegos", getSoloJuegos);
router.get("/soloJuego/:id", getSoloJuego);
// Get Jugadas
router.get("/juego/getLastJuego/:id", getLastJuego);
// Post Juegos
router.post("/juego", addJuego);
// Put Juegos
router.put("/juego/:id", updateJuego);
// Put Jugadas
router.put("/addUltimoJuego/:id", updateCreateLastGame);
// Delete Juegos
router.delete("/juego/:id", deleteJuego);
// Delete Jugadas
router.delete("/juego/:id/jugada", deleteJugada);

router.get("/jugadas/paginadas/:id?", getJugadasPaginadas);
router.get("/juego/jugadasPaginadas/:id?", getJuegoConJugadasPaginadas);

router.get("/juego/getAllJuego/:turno", getJuegosLastJugadaPorTurno);

router.put("/juego/updateActualJuego/:turno", updatePorTurnoJuegoActual);
export default router;
