import { deletePersonalizado, getPersonalizadoByParameterId, getPersonalizados, updateCreatePersonalizado } from "../controllers/personalizados";
import { Router } from "express";
const router = Router();

router.get("/getPersonalizado", getPersonalizados);
router.get("/getPersonalizado/:parametroId", getPersonalizadoByParameterId);

router.put("/putPersonalizado", updateCreatePersonalizado);

router.delete("/deletePersonalizado/:id", deletePersonalizado);

export default router;