import { IJuego } from "../models/juego.model";
import moment from "moment";
import Juego from "../models/juego.model";
import IJugadas from "../models/jugadas.model";
import { JugadaImpl } from "../models/jugadas.model";

export class GameplayCreator {
  private currentGameplays: IJuego[];
  private lastUpdate: moment.Moment | null | undefined;
  private gameplaysUpdateDelay = 1 / 4; //minutes

  public constructor() {
    this.currentGameplays = [];
  }

  public loop() {
    console.log({ message: "Gameplay Creator starting" });
    setInterval(() => this.process(), this.gameplaysUpdateDelay * 60000);
  }

  process(): void {
    console.log({ message: "New tick of Gameplay Creator" });
    if (
      this.lastUpdate == null ||
      this.lastUpdate == undefined ||
      this.lastUpdate.isBefore(
        moment().subtract(this.gameplaysUpdateDelay, "minutes")
      )
    ) {
      this.updateGameplays();
    }
    this.checkNextGameplays();
  }

  async updateGameplays() {
    console.log({ message: "Gameplay Creator decided to update games info" });
    this.currentGameplays = await Juego.find().select(["-jugadas"]);

    this.lastUpdate = moment();
  }

  checkNextGameplays() {
    console.log({
      message:
        "Gameplay Creator is setting timeouts to create gameplays in corresponding moment",
    });
    this.currentGameplays.forEach((g) => {
      var momnt = moment(g.horaDelSorteo.toString(), "hh:mm");
      if (momnt.isBefore(moment().add(this.gameplaysUpdateDelay, "minutes"))) {
        var wait = momnt.diff(moment());
        console.log({ message: "Scheduling a game", game: g, waitTime: wait });
        setTimeout(() => {
          this.createGameplayFor(g);
        }, wait);
      }
    });
  }

  async createGameplayFor(g: IJuego) {
    console.log(g.id);

    var noww = new Date();
    var today = new Date(noww.getFullYear(), noww.getMonth(), noww.getDate());
    var jugada = new JugadaImpl(today);

    const searchJuego = await Juego.findOne({
      "jugadas.fecha": today,
      _id: g.id,
    });

    //solo se crea si no existe

    if (!searchJuego && today.getDay() !== 0) {
      const juego = await Juego.findByIdAndUpdate(g.id, {
        $push: { jugadas: { $each: [jugada], $position: 0 } },
      });
    }
  }
}
