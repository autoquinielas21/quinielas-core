var http = require("http");

export class Imsomnia {
  public static keepWorking() {
    setInterval(() => {
      console.log("I need to keep awake to prevent horrible things to happen");
      http.get(process.env.APP_URL!);
    }, 1 * 60 * 1000);
  }
}
